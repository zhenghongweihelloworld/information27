
import random
import re
from datetime import datetime

from flask import request, abort, current_app, make_response, jsonify,session

from info.libs.yuntongxun.sms import CCP
from info.models import User
from info.utils.response_code import RET
from . import passport_blu

from info.utils.captcha.captcha import captcha
from info import redis_store, constants, db

@passport_blu.route("/logout")
def logout():
    """
    退出登录
    :return:
    """
    session.pop("user_id",None)
    session.pop("mobile",None)
    session.pop("nick_name",None)
    return jsonify(errno = RET.OK,errmsg = "推出成功")


@passport_blu.route("/login")
def login():
    """
    登录
    1获取参数
    2校验参数
    3校验密码是否正确
    4保存用户的登陆状态
    5响应
    :return:
    """
    params_dict= request.json
    mobile = params_dict.get("nobile")
    password = params_dict.get("password")

    if not all([mobile,password]):
        return jsonify(error=RET.PARAMERR, errmsg="参数有误")


    if not re.match('1[35678]\\d{9}', mobile):
        return jsonify(error=RET.PARAMERR, errmsg="手机号格式不正确")

    try:
        user = User.query.filter(User.mobile == mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(error=RET.DBERR, errmsg="数据查询失败")

    if not user:
        return jsonify(error=RET.NODATA, errmsg="用户不存在")

    if not user.check_passowrd(password):
        return jsonify(errno = RET.PWDERR,errsms= "用户名或者密码错误")

    #保持用户的登陆状态
    session["user_id"] = user.id
    session["mobile"] = user.mobile
    session["nick_name"] = user.nick_name

    #设置当前用户最后一次登录时间
    user.last_login = datetime.now()

    #如果在视图函数中,对模型身上的属性有修改,那么就要commit到数据库保存
    #但是其实可以不用自己去写 db.seesion.commit(),前提是对SQLALchemy有过相关操作


    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)

    #响应
    return jsonify(errno = RET.OK,errmsg = "登陆成功")










@passport_blu.route("/register",methods = ["post"])
def register():
    """
    注册逻辑
    1获取参数
    2校验参数
    3取到服务器保存的真实的短信验证码内容
    4校验用户输入的短信验证码内容和真实验证码内容是否一致
    5如果一致,初始化User模型,并且属性
    6将User添加到数据库
    7返回响应

    :return:
    """
    param_dict = request.json
    mobile = param_dict.get("mobile")
    smscode = param_dict.get("smscode")
    password = param_dict.get("password")

    if not all([mobile,smscode,password]):
        return jsonify(error=RET.PARAMERR,errmsg = "参数有误")
    if not re.match('1[35678]\\d{9}', mobile):
        return jsonify(error=RET.PARAMERR, errmsg="手机号格式不正确")
    try:
        real_sms_code = redis_store.get("SMS_"+mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(error=RET.DBERR, errmsg="数据查询失败")
    if not real_sms_code:
        return jsonify(error=RET.NODATA, errmsg="验证码已过期")
    if real_sms_code != smscode:
        return jsonify(error=RET.DATAERR, errmsg="验证码输入错误")
    #如果一致,初始化User模型,并且赋值属性
    user= User()
    user.mobile = mobile
    #暂时没有昵称,用手机号代替
    user.nick_name = mobile
    #记录用户最后一次的登录时间
    user.last_login = datetime.now()
    #对密码进行加密
    user.password = password

    #添加到数据库
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno = RET.DBERR,errmsg = "数据保存失败")
    #往session中保存数据表示当前已经登录
    session["user_id"] = user.id
    session["mobile"] = user.mobile
    session["nick_name"] = user.nick_name



@passport_blu.route("/sms_code")
def send_sms_code():
    """
    发送短信的逻辑
    1获取参数,手机号,图片验证码内容,图片验证码编号(随机值)
    2校验参数(参数是否符合规则,判断是否有值)
    3先从redis中取出真实的验证码内容
    4与用户的验证码对比,如果对比不一致,返回验证码输入错误
    5如果一致,生成验证码内容
    :return:
    """
    # 1获取参数, 手机号, 图片验证码内容, 图片验证码编号(随机值)
    params_dict = request.json
    mobile = params_dict.get("mobile")
    image_code = params_dict.get("image_code")
    image_code_id = params_dict.get("image_code_id")
    # 2校验参数(参数是否符合规则, 判断是否有值)
    if not all([mobile,image_code,image_code_id]):
        #{"error":"400","errmsg":"参数有误"}
        return jsonify(error=RET.PARAMERR,errmsg = "参数有误")
    if not re.match('1[35678]\\d{9}',mobile):

        return jsonify(error=RET.PARAMERR, errmsg="手机号格式不正确")


    # 3先从redis中取出真实的验证码内容
    try:
        real_iamge_code = redis_store.get("imageCodeId_"+image_code_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(error =RET.DBERR,errmsg="数据查询失败")
    if not real_iamge_code:
        return jsonify(error = RET.NODATA,errmsg = "验证码已过期")

    # 4与用户的验证码对比, 如果对比不一致, 返回验证码输入错误
    if real_iamge_code.upper != image_code.upper():
        return jsonify(error = RET.DATAERR,errmsg = "验证码输入错误")

    # 5如果一致, 生成验证码内容
    sms_code_str = "%06d"%random.randint(0,999999)

    #发送短信验证码
    result = CCP().send_template_sms(mobile,[sms_code_str,constants.SMS_CODE_REDIS_EXPIRES/5],"1")
    if result != 0:
        #代表发送不成功
        return jsonify(error =RET.THIRDERR,errmsg = "发送短信失败")
    #保存验证码到redis
    try:

        redis_store.set("SMS"+mobile,sms_code_str,constants.SMS_CODE_REDIS_EXPIRES)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(error = RET.DBERR,errsmg ="数据保存失败")

    #7告知发送结果
    return jsonify(error = RET.OK,errmsg = "发送成功")


@passport_blu.route("/image_code")
def get_image_code():
    """
    生成图片验证码,返回
    1取到参数
    2判断参数是否有值
    3生成图片验证码
    4保存图片验证码文字内容到redis
    5返回图片验证码
    :return:
    """
    #1取到参数
    image_code_id = request.args.get("imageCodeId",None)
    #2判断参数是否有值
    if not image_code_id:
        return abort(403)
    #3生成图片验证码
    name,text,image = captcha.generate_captcha()


    #4保存图片验证码文字内容到redis
    try:
        redis_store.set("imageCodeId_"+image_code_id,text,constants.IMAGE_CODE_REDIS_EXPIRES)
    except Exception as e:
        current_app.logger.error(e)
        abort(500)

    #5返回图片验证码
    #设置数据类型,以便浏览器识别
    response = make_response(image)
    response.headers["Content-Type"] = "image/jpg"

    return response
